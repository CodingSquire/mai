# Лабораторная работа по Linux:
## Вход в машину в сингл моде и смена пароля рута

Single mode

1) Открыть консоль VM
2) При загрузке выбираешь ядро - нажимаешь e - редактируешь параметр загрузки - вписываешь вместо ro
```rw init=/sysroot/bin/sh```
жмем ctrl+X
3)  Монтируем root
```root chroot /sysroot/```
4) После выполнения всех необходимых операций по настройке – перезагрузка.
```reboot –f```

---

## Смена пароля root

1) Открыть консоль
2) Перезагрузить сервер в меню старта GRUB нажать e
3) Убрать параметры **rhgb** и **quiet** из строки, которая начинается с linux16

**rd.break** – останавливает процесс загрузки до передачи управления от initramfs к systemd. Поэтому можно использовать initramfs для ввода команд.

**enforcing=0** - переключает систему безопасности SELinux в режим доверия. Это сохранит время для установки флагов безопасности для файловой системы, которое может потребоваться если SELinux отключен.

5) Нажать  **Ctrl+X**  для загрузки системы с изменёнными параметрами. Отобразится окно  switch_root prompt  initramfs.

Если ФС зашифрована 

Быстро нажимать backspace, чтобы скрыть системные информационные окна

6) Перемонтировать ФС (файловую систему) в режиме Write:

```
switch_root:/# mount -o remount,rw /sysroot

```

7) Перейти в chrooot: 

```
switch_root:/# chroot /sysroot
```

8) Теперь можно использовать passwd:

```
sh-4.2# passwd
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
```

9) Если появится ошибка Authentication token manipulation error, значит нужно перемонтировать sysroot см. Выше. – с разрешениями для записи.

![](https://gitlab.com/CodingSquire/mai/raw/master/labs/lab1/img/1.png)

![](https://gitlab.com/CodingSquire/mai/raw/master/labs/lab1/img/2.png)

![](https://gitlab.com/CodingSquire/mai/raw/master/labs/lab1/img/3.png)

![](https://gitlab.com/CodingSquire/mai/raw/master/labs/lab1/img/3_1.png)

---

## Cделать bash скрипт, который выясняет 5 самых "тяжелых" процессов и пишет их в лог каждые 10 секунд

```
> # cat top.sh
> #!/bin/bash
> while true
> do
>   date >> top.out
>   top -b -n 1| sed -n '/PID/,$p'| head -n 6 >> top.out
>   sleep 5
> done
>
>
> запускаем в background
> # sh top.sh &
> [1] 10151
>
>
> проверяем
> # tail -f top.out
> ..
> Ср ноя  6 02:05:19 MSK 2019
>   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
>  1931 velikan   20   0 3773288 262864  96972 S  23,5  3,4  19:35.70
> gnome-shell
>  2641 velikan   20   0  739152  48700  36328 S   5,9  0,6   0:21.58
> gnome-terminal-
>  2711 velikan   20   0 1590696 280884 138656 S   5,9  3,6   4:32.94 chrome
>  3540 velikan   20   0 1094464 241332  87708 S   5,9  3,1   1:26.56 chrome
>     1 root      20   0  239140  12400   7704 S   0,0  0,2   0:05.41 systemd
> Ср ноя  6 02:05:24 MSK 2019
>   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
>  1931 velikan   20   0 3773212 262764  96740 S  22,2  3,3  19:37.61
> gnome-shell
> 10194 root      20   0  257368   4344   3520 R  16,7  0,1   0:00.05 top
>  2711 velikan   20   0 1590696 280836 138656 S  11,1  3,6   4:33.52 chrome
>  3540 velikan   20   0 1094464 241588  87708 S  11,1  3,1   1:26.94 chrome
>  2003 velikan   20   0  566172  64532  42692 S   5,6  0,8   3:25.07
> Xwayland
> Ср ноя  6 02:05:29 MSK 2019
>   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
>  1931 velikan   20   0 3773212 262764  96740 R  52,6  3,3  19:39.53
> gnome-shell
> 10202 root      20   0  257368   4360   3536 R  26,3  0,1   0:00.08 top
>  2711 velikan   20   0 1590696 280836 138656 S  21,1  3,6   4:34.15 chrome
>  4259 velikan   20   0 2304240 117724  74416 S  21,1  1,5   4:25.48
> telegram-deskto
>  3540 velikan   20   0 1094464 242112  87708 S  15,8  3,1   1:27.37 chrome
> ..
>
>
> останавливаем
> # kill 10151
```


![](https://gitlab.com/CodingSquire/mai/raw/master/labs/lab1/img/4.png)

---

## Cкрипт должен запускаться и мониториться systemd юнитом, в случае падения - перезапускаться

Делаем описание сервиса:

```
/etc/systemd/system/top5.service
[Unit]
Description = Top 5 demo service
After=network.target
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/root/top.sh
[Install]
WantedBy=multi-user.target
```

**Запускаем** - systemctl start top5.service

**Смотрим статус** - systemctl status top5.service

**Убиваем процесс** – сервис автоматически поднимается.



![](https://gitlab.com/CodingSquire/mai/raw/master/labs/lab1/img/5.png)

---

## Cделать   снэпшот lvm партиции 

`lvcreate -s -L [размер снапшота] -n [имя снапшота] [lv диск с которого делается снапшот]`
 
`lvcreate -L10G -s -n snaphot_test.local /dev/vmstore/test.local`

`lvconvert --merge [путь к снапшоту]`

```
vm:~/# lvconvert --merge /dev/vmstore/snaphot_test.local 
File descriptor 7 (pipe:[35908221]) leaked on lvconvert invocation. Parent PID 19580: bash
  Merging of volume snaphot_test.local started.
  test.local: Merged: 91,3%
  test.local: Merged: 91,4%
  test.local: Merged: 91,7%
  test.local: Merged: 92,1%
  test.local: Merged: 92,5%
  test.local: Merged: 93,0%
  test.local: Merged: 93,6%
  test.local: Merged: 94,2%
  test.local: Merged: 94,7%
  test.local: Merged: 95,2%
  test.local: Merged: 95,8%
  test.local: Merged: 96,4%
  test.local: Merged: 96,7%
  test.local: Merged: 97,1%
  test.local: Merged: 97,5%
  test.local: Merged: 97,6%
  test.local: Merged: 97,7%
  test.local: Merged: 97,9%
  test.local: Merged: 98,1%
  test.local: Merged: 98,4%
  test.local: Merged: 98,5%
  test.local: Merged: 98,6%
  test.local: Merged: 98,9%
  test.local: Merged: 99,0%
  test.local: Merged: 99,1%
  test.local: Merged: 99,2%
  test.local: Merged: 99,4%
  test.local: Merged: 99,4%
  test.local: Merged: 99,5%
  test.local: Merged: 99,6%
  test.local: Merged: 99,9%
  test.local: Merged: 99,9%
  test.local: Merged: 100,0%
  Merge of snapshot into logical volume test.local has finished.
  Logical volume "snaphot_test.local" successfully removed
```

---

## Сделать форк бомбу и ограничить ее по ресурсам

**bomb1** - функция которая запускается рекурсивно

`bomb1 () {bomb1 | bomb1 &}; bomb`

**;** - **конец** определения функции

**&** - **запуск** функции в **фоне**, чтобы дочерние запуски открывались в отдельных процессах

**Рекурсия** - ` bomb | bomb` . Это значит перенаправить вывод новому вызову функции.

Чтобы **ограничить** нужно настроить кол-во процессов, которое разрешено выполнять в системе:

- ulimit - S - u 5000 (для сессии)
- pgrep -wcu $USER - покажет текущий лимит для пользователя

